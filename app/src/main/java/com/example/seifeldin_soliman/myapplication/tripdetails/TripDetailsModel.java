package com.example.seifeldin_soliman.myapplication.tripdetails;

import com.example.seifeldin_soliman.myapplication.Status;
import com.google.gson.annotations.SerializedName;

/**
 * Created by seifeldin_soliman on 1/6/2019.
 */

public class TripDetailsModel {

    @SerializedName("Status")
    private Status status;
    @SerializedName("TripDetail")
    private TripDetail tripDetail;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public TripDetail getTripDetail() {
        return tripDetail;
    }

    public void setTripDetail(TripDetail tripDetail) {
        this.tripDetail = tripDetail;
    }
}
