package com.example.seifeldin_soliman.myapplication.dailytrips;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/**
 * Created by seifeldin_soliman on 12/2/2018.
 */

public interface DailyTripsAPI {


    @GET("trips")
    Call<GetDailyTripModel> getTrips(@Header("Authorization") String uesr_id);
}
