package com.example.seifeldin_soliman.myapplication.login;

import com.example.seifeldin_soliman.myapplication.Status;
import com.google.gson.annotations.SerializedName;

/**
 * Created by seifeldin_soliman on 11/8/2018.
 */

public class UserLoginModel {

    @SerializedName("Status")
    Status status;
    @SerializedName("User")
    User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}
