package com.example.seifeldin_soliman.myapplication.triphistory;

import com.example.seifeldin_soliman.myapplication.Status;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by seifeldin_soliman on 12/2/2018.
 */

public class GetTripHistoryModel {

    @SerializedName("Status")
    Status status;
    @SerializedName("TripHistory")
    ArrayList< TripHistory> tripHistory;
}
