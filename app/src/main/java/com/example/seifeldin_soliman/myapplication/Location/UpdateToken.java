package com.example.seifeldin_soliman.myapplication.Location;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by seifeldin_soliman on 12/26/2018.
 */

public class UpdateToken extends com.google.firebase.iid.FirebaseInstanceIdService{


    @Override
    public void onTokenRefresh() {
        String token = FirebaseInstanceId.getInstance().getToken();

        Log.d("RefreshToken", "onTokenRefresh: "+token);
    }
}
