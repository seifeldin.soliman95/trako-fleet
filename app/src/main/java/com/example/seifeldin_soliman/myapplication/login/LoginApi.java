package com.example.seifeldin_soliman.myapplication.login;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by seifeldin_soliman on 11/8/2018.
 */

public interface LoginApi {
    @FormUrlEncoded
    @POST("login")
    Call<UserLoginModel> getTripInfo(@Field("email") String email,
                                     @Field("password") String password,
                                     @Field("FCMToken")String FCMToken);


}
